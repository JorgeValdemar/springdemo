/**
 * 
 */
package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.Contato;

/**
 * @author teste
 *
 */
public interface Contatos extends JpaRepository<Contato, Long> {

}
