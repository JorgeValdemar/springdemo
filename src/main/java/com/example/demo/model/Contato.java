package com.example.demo.model;

import java.io.Serializable;

import javax.persistence.*;

@Entity
public class Contato implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
   
	private String nome;
   
	private String email;

	public Long getId() {
		return this.id;
	}

	public String getNome() {
		return this.nome;
	}

	public String getEmail() {
		return this.email;
	}

	public void setId(Long value) {
		this.id = value;
	}

	public void setNome(String value) {
		this.nome = value;
	}

	public void setEmail(String value) {
		this.email = value;
	}
	
}
